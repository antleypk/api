#!/bin/bash
set -exou pipefail

pip3 install gunicorn || echo "gunicorn failed"
pip3 install flask  || echo "flask failed"
pip3 install flask_cors || echo "flask_cors failed"
