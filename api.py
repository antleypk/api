from flask import Flask, request, jsonify, redirect, send_file
import logging
from flask_cors import CORS, cross_origin
from pathlib import Path
import sys, os, time, json

from datetime import datetime
app = Flask(__name__)

@app.route("/test", methods=["POST", "GET"])
@cross_origin()
def test():
    print("test")
    return make_response({"working": 1, })

def make_response(jdat):
    response = jsonify(jdat)
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=443, debug=True)